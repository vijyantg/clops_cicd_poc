![Logo](https://media-exp1.licdn.com/dms/image/C560BAQH7KkzlahxISw/company-logo_200_200/0/1519865199863?e=1668038400&v=beta&t=t1mSQldU9G-zJu4efU5_vUYkfOG71KdgGRFEqBg9OFo)
# Continuous Integration using Circle CI

CircleCI is preferred for its parallel execution of builds, maintenance, interactive user interface, managed cloud hosting, and faster deployment jobs.
It is a widely used tool to run complex pipelines for efficient automated testing and continuous integrations that run on cloud-based systems or self-hosted infrastructure.
Their platform has become the prime contributor to the advancement of DevOps.
## Sign up in your CircleCI account
CircleCI provides its cloud-hosted servers that are almost free with limitations like builds minutes for every month and repository storage.   
These limitations can be released as we upgrade to paid servers. On the other hand, we have the freedom to host servers on-prem or on private clouds.
You have full access to unlimited build minutes and storage and the freedom to tweak your servers at your will. 

 - [Cloud-based CircleCI servers](https://circleci.com/signup)
 - [Self-Hosted or Enterprise  ](https://circleci.com/enterprise-trial-install/)


## Prerequisites 

We are going to have a demo,on how CircleCI integrates with VCS and other tools and how its pipeline works.
We are going to use 

`vcs`:  this is where our configuration file will be stored  

`dockerhub`: this is where our built image will be pushed

`AWS account`:  To create and deploy eks cluster

`sonarcloud`: to scan for code analysis and code quality

`trivy`: to scan built images for vulnerability


### Version Control System

CircleCI integrates with repositories for source code management.
To this day, CircleCI accepts GitHub ,Gitlab and Bitbucket.

## Before We Start

We need to learn few concepts and keywords that are going to help us to understand following pipeline working.
We have sample pipeline code in our [Gitlab Repo](https://gitlab.cloudifyops.com/devops-toolset/clops-cicd-code-repo.git) that would be cloned/fetched/checkout to CircleCI working directory.
We have now repo in our workspace ,we will scan the repo with [sonarcloud](http://sonarcloud.io/) for code quality checks and then move to next stage to build image. To check that our built image has any vuneralibilties, we are going to pass it through `trivy` image scanner. Once docker image is passed, tag it and login to dockerhub to push image.
Next step would be to deploy this container image to AWS EKS cluster.
Will learn about each steps in details.    

`Note` In most of the pipeline architecture, we need runners to execute the jobs. Luckily, CircleCI provides in-built Runners with limitaions. In case, you want to try follow this 
[self-hosted runner](https://circleci.com/docs/runner-installation) and execute jobs on your infrastructure. 
## Core Concepts

### Configuration 

Circle CI/CD is entirely processed as `configuration as code` and written in easy to understand YAML file.
The `config.yml` file is located in a folder called `.circleci` at the root of your project

You can orchestrate the entire CircleCI using the configuration file.
It can be adapted to fit many different needs of your project.
We will learn key components of the configuration file in order of granularity.

`Pipeline` entire configuration in config.yaml is Pipeline 

`Workflows` master the pipeline on how multiple jobs are to be executed i.e., run order, approvals, etc

`Jobs` are the building blocks of the pipeline. they define how and under what conditions they should be executed 

`Steps` In each job, you can mention `run: |` command to install dependencies or run shell scripts

### Contexts

On your Dashboard,go to  `Organization Settings -> Contexts`.
Context helps you to provide a medium for secured sharing of environment variables across the project.
After a context is created, you can use the `context key` to give any job(s) access to the environment variables associated with that context.

### Workspace

Data persistence is a way to move data between jobs and reduce the time for the build.
Using workspace is one way for it apart from caches and artifacts.
Workspace stores data of the job and is unique which may come to carry out other jobs.

### Docker Layer Caching

Whether we import or build docker images it contains layers. Each layer contains the filesystem changes to the image for the state before the execution of the command and the state after the execution of the command. Docker uses a layered cache to optimize and speed up the process of building Docker images.
It speeds up Docker builds due to reusing previously created layers if they are not altered.
Setting `docker_layer_caching: true` will reuse previously build layers thus saving a lot of processing.

## Execution environment

Each separate job(s) defined inside your config file runs in a unique execution environment so-called `executors`.
This executor can be a Docker container or a virtual machine running linux, windows, or mac.
You can make an image as each executor. CircleCI provides a range of public images that contain sets of instructions for creating a running container or VMs.
Please follow `[link]reference`
 for convenience images for CircleCI in the reference section. 
For added security when using the Docker executor and running Docker commands, the `setup_remote_docker` key can be used to spin up another Docker container in which to run these commands.  

`Note` circle ci has depreciated images with the prefix "circleci". Images with `cimg` are currently in use.

### Images

As we already discussed what are images, we know CircleCI maintains convenience images for popular languages on Docker Hub giving you access to limited tasks that are stored in the container.
And when machine executors are considered they spin up a complete VM image giving you full access to the os resource and control over the job environment.

### Jobs

Jobs are a set of steps that run commands/scripts as required. Jobs must specify an executor that could be docker images or any VMs.
In docker, if you don't specify the image version it will pick up the latest available image from docker hub.
For mac and windows, you can use `Xcode` and `windows orb` respectively.

### Orbs 

Orbs are an important part of CircleCI pipelines. They are open community.
It is a reusable block of code that helps in automating repeated tasks and speeds up project setup therefore make s it easy to integrate with third-party tools.
Please follow the `reference` for orbs.

### Parallelism 

It's a plus to keep Parallelism in practice in any pipeline as per business standards.
The more tests your project involves, the longer it takes for them to compute on a single machine.
Using parallelism, you can spread your tests across the said number of executors. 

### Resource Class

A configuration option that gives you control to opt to compute resources for your jobs.
Every executor comes with a default resource class but it is always best practice to mention it in the pipeline.
Pricing and plans differ according to the resource type that you opt for.
Refer to [Resource Classes](https://circleci.com/product/features/resource-classes/) for better understanding.




### Sonarcloud

[Sonarcloud](http://sonarcloud.io/) is cloud based code analysis and code quality service.
Sonarcloud is an automatic code review tool to detect bugs, vulnerabilities, and code smells in your code. It can integrate with your existing workflow to enable continuous code inspection across your project branches and pull requests.

No Installation is required. Sign up with git repo account.
Follow these [instructions](https://docs.sonarcloud.io/getting-started/gitlab/) to get started. 

### Trivy 

[Trivy](https://github.com/aquasecurity/trivy) is a comprehensive security scanner. It is reliable, fast, extremely easy to use, and it works wherever you need it.
See [installation](https://aquasecurity.github.io/trivy/v0.30.4/getting-started/installation/) methods to know different ways you can scan your image.

Using  docker trivy container scan the image you want 
`docker run aquasec/trivy image DOCKERUSER/REPOSITORY:${TAG}` 

`Note` trivy may not generate any report because the OS is missing.
## Environment Variables

To run this project, you will need to add the following environment variables in `Project Settings -> Environment Variables`.
These are confidential variables stored in key/value format.
Such variables are not hardcoded in the pipeline so when their is any logs stored your variables are not open to eyes.
 
`AWS_ACCESS_KEY_ID`

`AWS_REGION	`

`AWS_SECRET_ACCESS_KEY`

`DOCKER_PASS`

`USERNAME`

### Contexts 

`SONAR_TOKEN`




##  Code snippets 

### Sonarcloud to scan the code quality

```yaml
version: 2.1  
orbs:                                                                      
    sonarcloud: sonarsource/sonarcloud@1.1.1
jobs:
    buildanddeploy:
        docker :
            - image: cimg/deploy:2022.07
        working_directory: ~/workspace/circleci
        steps:
            - checkout 
            - sonarcloud/scan
workflows: 
    version: 2 
    build:
        jobs:  
            - buildanddeploy:
                context: SonarCloud
          

```
### Docker build from repository

```yaml
version: 2.1  
jobs:
    buildanddeploy:
        docker :
            - image: cimg/deploy:2022.07
        working_directory: ~/workspace/circleci
        steps:
            - checkout
            - setup_remote_docker:
                version: 20.10.14
                docker_layer_caching: true
            - run: |            
                TAG=0.1.${CIRCLE_BUILD_NUM}        
                docker build -t DOCKERUSER/REPOSITORY:${TAG} .
workflows: 
    version: 2 
    build:
        jobs:  
            - buildanddeploy                

```
### Scan with trivy for vulnerability

```yaml
version: 2.1  
jobs:
    buildanddeploy:
        docker :
            - image: cimg/deploy:2022.07
        working_directory: ~/workspace/circleci
        steps:
            - checkout
            - run: |                   
                docker run aquasec/trivy image DOCKERUSER/REPOSITORY:${TAG}
                
workflows: 
    version: 2 
    build:
        jobs:  
            - buildanddeploy
```
### Push built image to dockerhub

```yaml
version: 2.1  
jobs:
    buildanddeploy:
        docker :
            - image: cimg/deploy:2022.07
        working_directory: ~/workspace/circleci
        steps:
            - checkout
            - setup_remote_docker:
                version: 20.10.14
                docker_layer_caching: true
            - run: |            
                TAG=0.1.${CIRCLE_BUILD_NUM}        
                docker build -t DOCKERUSER/REPOSITORY:${TAG} .
                echo $DOCKER_PASS | docker login -u $USERNAME --password-stdin
                docker push DOCKERUSER/REPOSITORY:${TAG}

workflows: 
    version: 2 
    build:
        jobs:  
            - buildanddeploy
```
### Deploy To AWS EKS Cluster 

```yaml
version: 2.1  
jobs:
    buildanddeploy:
        docker :
            - image: cimg/deploy:2022.07
        working_directory: ~/workspace/circleci
        steps:
            - checkout
            - setup_remote_docker:
                version: 20.10.14
                docker_layer_caching: true
            - run: |         
                sudo apt-get update -y    
                curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"                              
                unzip awscliv2.zip 
                sudo ./aws/install
                curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
                sudo mv /tmp/eksctl /usr/local/bin
                curl -o kubectl https://s3.us-west-2.amazonaws.com/amazon-eks/1.22.6/2022-03-09/bin/linux/amd64/kubectl        
                chmod +x ./kubectl
                mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
                aws eks --region AWS-REGION update-kubeconfig --name CLUSTER-NAME                                              
                kubectl apply -f service.yaml
                kubectl apply -f deployment.yaml

workflows: 
    version: 2 
    build:
        jobs:  
            - buildanddeploy
```
`Note` 
We could have used [AWS docker image](https://circleci.com/developer/images/image/cimg/aws) for pre-installed aws essential packs
    
## Screenshots

![How Workspace works ](https://circleci.com/docs/assets/img/docs/workspaces.png) 

![Pipeline UI](https://gitlab.cloudifyops.com/devops-toolset/clops-cicd-code-repo/-/raw/main/CIRCLECI/Screenshots/Screenshot__206_.png)

![Build UI](https://gitlab.cloudifyops.com/devops-toolset/clops-cicd-code-repo/-/raw/546f9c15173d51f1809b24f7f9f36b13bff2853c/CIRCLECI/Screenshots/Screenshot__208_.png)


## Features

- Auto-cancel redundant, queued or running builds on GitHub when a newer build is triggered on that same branch(except the default branch).
- PR-only build feature allows running builds when there’s an open pull request.
- Parallelism splits and balances tests across multiple containers to decrease overall build time.
- Additional Admin-only Permissions and prohibit non-admins from modifying critical project settings.
- SSH into running builds since the container persists for 30 minutes while you securely troubleshoot irregularities.
- Quick access and setup through Web App



## Authors & Approvals

- ?


## References

Here are some related links which you may find useful.

- [For Self-Hosted Solution](https://circleci.com/pricing/server/)

- [Runners](https://circleci.com/docs/runner-overview)

- [Onboard with Web Applications: Know your Workaround](https://circleci.com/docs/introduction-to-the-circleci-web-app)

- [To get the most out of Configuration File](https://circleci.com/docs/configuration-reference)

- [Read more about restriction in Context](https://circleci.com/docs/contexts)

- [Persisting data in Persisting data in workflows: when to use caching, artifacts, and workspaces](https://circleci.com/blog/persisting-data-in-workflows-when-to-use-caching-artifacts-and-workspaces/)

- [Go read about Docker Layer Caching](https://circleci.com/docs/docker-layer-caching)

- [Instead of creating .config.yml for every pipeline,try considering Dynamin Configuration](https://circleci.com/docs/using-dynamic-configuration)

- [Executors as Docker ,VM ,macOS …](https://circleci.com/docs/executor-intro)

- [Convenience Images](https://circleci.com/developer/images)

- [Follow parallelism if heavy computation is required](https://circleci.com/docs/parallelism-faster-jobs)

- [CircleCI vs GitlabCI](https://circleci.com/circleci-versus-gitlab/)









