![Logo](https://media-exp1.licdn.com/dms/image/C560BAQH7KkzlahxISw/company-logo_200_200/0/1519865199863?e=1668038400&v=beta&t=t1mSQldU9G-zJu4efU5_vUYkfOG71KdgGRFEqBg9OFo)

# Continuous Integration using Github Actions

## Overview

GitHub Actions is a continuous integration and continuous delivery tool that allows you to automate your build, test, and deployment pipeline. You can create workflows using configuration as code that build and test every pull request to your repository, or deploy merged pull requests to production.
It provides Linux, Windows, and macOS virtual machines to run your workflows, or you can host your own self-hosted runners in your own data center or cloud infrastructure.
  
## Sign Up with GitHub

Github Actions comes with Github Source Code Management. You can find Actions on your dashboard of your Repository.
[Sign Up](https://github.com/signup?ref_cta=Sign+up&ref_loc=header+logged+out&ref_page=%2F&source=header-home) with your account to start using GitHub Actions.
GitHub offers tiers of suscription depending upon one's need. Github also offers trial pack for Enterprise Edition.
While in Free verion, you get 2000 CI/CD minutes/month of build and 500 MB of Packages storage. 
Please refer to [pricing](https://github.com/pricing#compare-features) to compare features between plans.
We will be using Free plan for our demo.
## Prerequisites

For our Demo, Github Actions will be requiring:

`Version Control System`: This is where our configuration file will be stored which is github itself 

`Self-Hosted Runner`: Install linux-x64 based runner on AWS EC2

`Docker Hub`: This is where our built image will be pushed

`AWS`:  To create and deploy EKS Cluster

`Sonarcloud`: To scan for code analysis and code quality

`Trivy`: To scan built images for vulnerability

`Anchore`: An alternative to Trivy 


### Migration

GitHub offers to migrate from various CI/CD tools as they share several configuration similarities and create workflows that automatically build, test, publish, release, and deploy code.
[GitlabCI](https://docs.github.com/en/actions/migrating-to-github-actions/migrating-from-gitlab-cicd-to-github-actions) , [CircleCI](https://docs.github.com/en/actions/migrating-to-github-actions/migrating-from-circleci-to-github-actions) and [Jenkins](https://docs.github.com/en/actions/migrating-to-github-actions/migrating-from-jenkins-to-github-actions) ,.etc are some tools.


## Installation

GitHub Action is a feature of GitHub which is already hosted on It.
Create your repo in GitHub , Actions option will be vsisble in the Dashboard. 
It is enabled by default, If not go to repo `Settings -> Actions -> General -> allow all actions and workflows`.

### Runners

Runners are the machines that execute jobs in a GitHub Actions workflow. GitHub provides runners that you can use to run your jobs, or you can host your own runners. Each [GitHub-hosted](https://docs.github.com/en/actions/using-github-hosted-runners/about-github-hosted-runners) runner is a new virtual machine (VM) hosted by GitHub with the runner application and other tools preinstalled, and is available with Ubuntu Linux, Windows, or macOS operating systems. When you use a GitHub-hosted runner, machine maintenance and upgrades are taken care of for you.
While [Self-Hosted](https://docs.github.com/en/actions/hosting-your-own-runners/adding-self-hosted-runners) runners gives you freedom to use unlimited build minutes , more control of hardware , operating system and many more only you need to be responsible for cost of the infrastructure.

You can choose to define by specifing whether to use gitlab hosted servers like `runs-on: ubuntu-latest` or self-hosted servers by `runs-on: [self-hosted, linux]`. 
Read more about runners in Reference.

In our demo we will be using self hosted infrastructure, to install runner we will go to    `repository -> settings -> Actions -> Runners `. 

Click on New Self-hosted runner -> Choose your OS / Runner Image -> Select Architecture. 
We will use Linux with x64 and run the following script in your EC2.

#### Download
```bash

# Create a folder
mkdir actions-runner && cd actions-runner

# Download the latest runner package
curl -o actions-runner-linux-x64-2.294.0.tar.gz -L 
https://github.com/actions/runner/releases/download/v2.294.0/actions-runner-linux-x64-2.294.0.tar.gz

# Optional: Validate the hash
echo "a19a09f4eda5716e5d48ba86b6b78fc014880c5619b9dba4a059eaf65e131780  actions-runner-linux-x64-2.294.0.tar.gz" | shasum -a 256 -c

# Extract the installer
tar xzf ./actions-runner-linux-x64-2.294.0.tar.gz
```

#### Configure
```bash

# Create the runner and start the configuration experience
./config.sh --url YOURREPO --token TOKENID

# Last step, run it!
./run.sh
```

#### Using your self-hosted runner

```bash
# Use this YAML in your workflow file for each job
runs-on: self-hosted
```


## Before We Start

We need to learn few concepts and keywords that are going to help us to understand following pipeline workflow.
We have sample pipeline code in our [Gitlab Repo](https://gitlab.cloudifyops.com/devops-toolset/clops-cicd-code-repo/-/tree/main/GITHUB%20ACTION) that would be cloned to runner workspace.
We have now repo in our workspace ,we will scan the repo with [Sonarcloud](http://sonarcloud.io/) for code quality checks and then move to next stage to build image. We will build a test container on local workspace and check that our built conatainer has any vuneralibilties, we are going to pass it through [anchore scan](https://github.com/marketplace/actions/anchore-container-scan).  
 Once docker image is passed, tag it and login to dockerhub to push image.
Next step would be to deploy this container image to    `AWS EKS cluster`.
Will learn about each steps in details.

# Core Concepts 

## Configuration 

Github Action CI/CD workflows are run as `configuration as code` and are written in easy to understand YAML file and a repository can have multiple workflows, each of which can perform a different set of tasks.
The `config.yml` file is located in a folder `.github/workflows` at the root of your project.
You can configure a GitHub Actions workflow to run when an event occurs in your repository, such as a pull request being opened or an issue being created. Your workflow contains one or more tasks that can run in sequential order or in parallel. Each task will run inside its own runner virtual machine or inside a container and has one or more steps that either run a script you define or run an action, a reusable extension that can simplify your workflow.

## Events
An `event` is a specific activity in a repository that triggers a workflow run.
You can trigger a workflow run on a schedule or by posting to a REST API.
## Jobs

A `job` is a set of steps in a workflow that execute on the same runner. 
Each steps either contains run/shell script or pre-made Actions. They execute in order and are dependent.
Steps shares date with each other and you can configure job's dependecies with other jobs but you cannot use until the dependent job finishes.
We have used test image to scan for vuneralibilties.Refer to workflow.

## Actions  

An `action` is a pre-made custom functions for the GitHub Actions that helps to reduce the amount of repetitive code that you write in your workflow files.
They can pull your git repository, set up required toolchain for your build, or set up the authentication to your cloud providers or Docker or any.
You can custom built your [own](https://docs.github.com/en/actions/creating-actions) actions or find in [marketplace](https://github.com/marketplace?type=actions).


## About .yml syntax

- ### `name`
The name of the workflow as it will appear in the Actions tab of the GitHub repository.
If you omit name, GitHub sets it to the workflow file path relative to the root of the repository.

- ### `on`
To automatically trigger a workflow, use on to define which events can cause the workflow to run.
We used `push` and `pull request` events , so a workflow run is triggered every time someone pushes a change to the repository or merges a pull request. This is triggered by a push or PR to `main` branch only.

- ### `uses`
This keyword specifies that this step will run latest version of the action.
`actions/checkout` checks out your repository onto the runner, allowing you to run scripts or other actions against your code 
You can give a `name` to the step and you can use `with` to give parameters for the action.

- ### `env`
Environment variables are available to the steps of all jobs in the workflow. You can also set environment variables that are only available to the steps of a single job or to a single step.
To get access and integrate with other 3rd party tools we need to provide `tokenid`. In order to avoid hardcoding of our credentials or confidential data, we pass our data through `secrets`.

- ### `secrets`
Secrets are environment variables that are encrypted.
Thet are not visible in the logs or anywhere. Anyone with collaborator access to this repository can use these secrets for Actions.
You can find secrets in     `settings -> under security -> secrets`



### Environment Variabales 

`USERNAME` `DOCKERPASS`   `SONAR_TOKEN`  `TOKEN_GITHUB`


### Integrations

#### Sonarcloud

[Sonarcloud](http://sonarcloud.io/) is cloud based code analysis and code quality service.
Sonarcloud is an automatic code review tool to detect bugs, vulnerabilities, and code smells in your code. It can integrate with your existing workflow to enable continuous code inspection across your project branches and pull requests.

No Installation is required. Sign up with git repo account.
Follow these [instructions](https://docs.sonarcloud.io/getting-started/gitlab/) to get started. 

#### (Optional) Trivy 

[Trivy](https://github.com/aquasecurity/trivy) is a comprehensive security scanner. It is reliable, fast, extremely easy to use, and it works wherever you need it.
See [installation](https://aquasecurity.github.io/trivy/v0.30.4/getting-started/installation/) methods to know different ways you can scan your image.

Using  docker trivy container scan the image you want 
`docker run aquasec/trivy image DOCKERUSER/REPOSITORY:${TAG}`
## Code Snippets

### Scan Repo with sonarcloud

```yaml

name: Scan Repo with Sonarcloud 
on:
  push:
    branches: [main]
  pull_request:
    branches: [main]

jobs:  
  build:  
    runs-on: [ self-hosted , Linux ]     
    steps:
    - 
      uses: actions/checkout@v2
      with:
          fetch-depth: 0
    -
      name: Set up Docker Buildx
      uses: docker/setup-buildx-action@v2
 
    - 
      name: SonarCloud Scan
      uses: SonarSource/sonarcloud-github-action@v1.6
      env:
        GITHUB_TOKEN: ${{ secrets.TOKEN_GITHUB}}
        SONAR_TOKEN: ${{secrets.SONAR_TOKEN}} 
```
### Scan Container with Anchore
```yaml
name: Build And Push to Dockerhub 
on:
  push:
    branches: [main]
  pull_request:
    branches: [main]

jobs:  
  build:  
    runs-on: [ self-hosted , Linux ]     
    steps:
    - 
      uses: actions/checkout@v2
      with:
          fetch-depth: 0 
   
    - name: build local container
      uses: docker/build-push-action@v2
      with:
        tags: localbuild/testimage:latest
        push: false
        load: true

    - name: Scan image
      uses: anchore/scan-action@v3
      with:
        image: "localbuild/testimage:latest"
        severity-cutoff: critical
        fail-build: true
        # fail-build-on-failure: true
```

### Build and Push to Dockerhub
```yaml

name: Build with Docker and AWS 
on:
  push:
    branches: [main]
  pull_request:
    branches: [main]

jobs:  
  build:  
    runs-on: [ self-hosted , Linux ]     
    steps:
    - 
      uses: actions/checkout@v2
      with:
          fetch-depth: 0 # Shallow clones should be disabled for a better relevancy of analysis
    -
      name: Set up Docker Buildx
      uses: docker/setup-buildx-action@v2
   
    - 
      name: Login to Docker Hub
      uses: docker/login-action@v2
      with:
        username: ${{ secrets.USERNAME }}
        password: ${{ secrets.DOCKERPASS }}    
    -
      name: Build and push
      uses: docker/build-push-action@v3
      with:
        push: true
        tags: DOCKERUSER/REPOSITORY:action.${{ steps.commit.outputs.short }}
```

```yaml
```
## Screenshots

![Runner UI](https://gitlab.cloudifyops.com/devops-toolset/clops-cicd-code-repo/-/raw/main/GITHUB%20ACTION/Screenshots/Screenshot__212_.png)
![Build UI](https://gitlab.cloudifyops.com/devops-toolset/clops-cicd-code-repo/-/raw/main/GITHUB%20ACTION/Screenshots/Screenshot__216_.png)


## Features

- Actions supports the core languages ​​C, C++, C#, Java, JavaScript, PHP, Python, Ruby, Scala and TypeScript, but with QEMU to use any of your preferred languages ​​with GitHub actions.
- Multi-container testing: Test your web service and its database in your workflow by simply adding docker-compose to your workflow file.
- Actions offers the flexibility to automate any webhook. Webhooks are automatic messages sent from applications when something happe-. They have a message or any content ​​and are sent to a unique URL essentially a phone number or app address.
- GitHub Actions for app build monitoring, performance measurement, bug tracking, and more through integrations with third-party too-. GitHub Actions also creates live logs that let you watch your workflows run in real time. Live logs also give you the option to copy a link from a failed step to identify and resolve potential issues. 
## References


[Finding and customizing actions](https://github.com/marketplace?type=actions)

[Github Products](https://docs.github.com/en/get-started/learning-about-github/githubs-products)

[Sign up for enterprise cloud or server](https://github.com/organizations/enterprise_plan?ref_cta=Start%2520a%2520free%2520trial&ref_loc=billboard&ref_page=%2F)

[About Self Hosted Runners](https://docs.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners#differences-between-github-hosted-and-self-hosted-runners)

[Github vs self](https://docs.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners#differences-between-github-hosted-and-self-hosted-runners)

[Github-hosted](https://docs.github.com/en/actions/using-github-hosted-runners/about-github-hosted-runners)

[Using workflows](https://docs.github.com/en/actions/using-workflows)


[Using workflows](https://docs.github.com/en/actions/using-workflows)

[Using jobs - GitHub Docs](https://docs.github.com/en/actions/using-jobs)

[Workflow syntax](https://docs.github.com/en/actions/using-workflows/workflow-syntax-for-github-actions)

[Using environments for jobs](https://docs.github.com/en/actions/using-jobs/using-environments-for-jobs)

[Caching dependencies to speed up workflows](https://docs.github.com/en/actions/using-workflows/caching-dependencies-to-speed-up-workflows)
